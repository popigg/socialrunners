<?php

namespace Application\Form; 

use Zend\Captcha; 
use Zend\Form\Element; 
use Zend\Form\Form; 

class NewsletterSubscriptionForm extends Form 

{ 
    public function __construct($name = null) 
    { 
    	parent::__construct(''); 

    	$this->setAttribute('method', 'post'); 
        $this->setAttribute('action', 'http://gmail.us7.list-manage2.com/subscribe/post?u=049bc82d30e4869d740d24d3a&amp;id=8f79f59530');
        
    }
}
