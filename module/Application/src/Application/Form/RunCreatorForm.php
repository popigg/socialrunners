<?php

namespace Application\Form; 

use Zend\Captcha; 
use Zend\Form\Element; 
use Zend\Form\Form; 

class RunCreatorForm extends Form 

{ 
    public function __construct($name = null) 
    { 
        parent::__construct(''); 
        
        $this->setAttribute('method', 'post'); 
        $this->setAttribute('action', 'create');
        $this->add(array( 
            'name' => 'place', 
            'type' => 'Zend\Form\Element\Text', 
            'attributes' => array( 
                'placeholder' => 'Place', 
                'required' => 'required', 
            ), 
            'options' => array( 
            ), 
        )); 
 
        $this->add(array( 
            'name' => 'date', 
            'type' => 'Zend\Form\Element\Date', 
            'attributes' => array( 
                'placeholder' => 'Date', 
                'required' => 'required', 
                'min' => '1970-01-01', 
                'max' => '2013-7-3', 
                'step' => '1', 
            ), 
            'options' => array( 
            ), 
        )); 
 
        $this->add(array( 
            'name' => 'time', 
            'type' => 'Zend\Form\Element\Time', 
            'attributes' => array( 
                'placeholder' => 'Time', 
                'required' => 'required', 
                'min' => '2013-01-01', 
                'max' => '2013-7-3', 
                'step' => '1', 
            ), 
            'options' => array( 
            ), 
        )); 
 
        $this->add(array( 
            'name' => 'distance', 
            'type' => 'Zend\Form\Element\Text', 
            'attributes' => array( 
                'placeholder' => 'Distance', 
                'required' => 'required', 
            ), 
            'options' => array( 
            ), 
        )); 

        $this->add(array( 
            'name' => 'duration', 
            'type' => 'Zend\Form\Element\Text', 
            'attributes' => array( 
                'placeholder' => 'Duration', 
                'required' => 'required', 
            ), 
            'options' => array( 
            ), 
        ));
 
        $this->add(array( 
            'name' => 'pace', 
            'type' => 'Zend\Form\Element\Text', 
            'attributes' => array( 
                'placeholder' => 'Pace', 
            ), 
            'options' => array( 
            ), 
        )); 
 
        $this->add(array( 
            'name' => 'gpx', 
            'type' => 'file', 
            'attributes' => array(  
            ), 
            'options' => array( 
                'label' => 'gpx', 
            ), 
        )); 
 
        $this->add(array( 
            'name' => 'meetingPoint', 
            'type' => 'Zend\Form\Element\Text', 
            'attributes' => array( 
                'placeholder' => 'Meeting point', 
                'required' => 'required', 
            ), 
            'options' => array( 
            ), 
        )); 
 
        $this->add(array( 
            'name' => 'description', 
            'type' => 'Zend\Form\Element\Textarea', 
            'attributes' => array( 
            ), 
            'options' => array( 
            ), 
        )); 
 
        $this->add(array( 
            'name' => 'drinks', 
            'type' => 'Zend\Form\Element\Text', 
            'attributes' => array( 
                'placeholder' => 'Drinks after at ...', 
            ), 
            'options' => array( 
            ), 
        )); 

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Submit',
            ),
        ));
 
        $this->add(array( 
            'name' => 'csrf', 
            'type' => 'Zend\Form\Element\Csrf', 
        ));        
    } 
}
