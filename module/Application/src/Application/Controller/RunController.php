<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Doctrine\ORM\EntityManager;
use Application\Entity\Run;
use Application\Form\RunCreatorForm;

/**
 * Class RunController
 * @package Application\Controller
 */
class RunController extends AbstractActionController
{    
    /**
     * @var Doctrine\ORM\EntityManager
     */
    protected $em;

    /**
     * @return Doctrine\ORM\EntityManager|array|object
     */
    public function getEntityManager()
    {
        if (null === $this->em) {
            $this->em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        }
        return $this->em;
    }

    /**
     * @return array|ViewModel
     */
    public function indexAction()
    {
        $run = $this->getEntityManager()->getRepository('Application\Entity\Run');
        $runList = $run->listRun();
        return new ViewModel(array('list' => $runList));
    }

    /**
     * @return ViewModel
     */
    public function createAction()
    {
        $form = new RunCreatorForm();
        $run = $this->getEntityManager()->getRepository('Application\Entity\Run');
        $request = $this->getRequest();
        if ($request->isPost()) {
            $runInfo = $request->getPost();
            $runInfo->user_id = 1;
            $run->addRun($runInfo);
            return new ViewModel(array('form' => $form, 'inserted' => true));
        }

        return new ViewModel(array('form' => $form));
    }
}
