<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Doctrine\ORM\EntityManager;
use Application\Entity\User;

class LoginController extends AbstractActionController
{
	/**
     * @var Doctrine\ORM\EntityManager
     */
    protected $em;
 
    public function setEntityManager(EntityManager $em)
    {
        $this->em = $em;
    }
 
    public function getEntityManager()
    {
        if (null === $this->em) {
            $this->em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        }
        return $this->em;
    }
    
    public function fbloginAction()
    {
        $facebook = new \Facebook(array(
             'appId' => 'XXXXXXXXXXXX',
             'secret' => 'XXXXXXXXXXXXX',
             ));
        $user = $facebook->getUser();
        if(!empty($user)){
            try {
                $userProfile = $facebook->api('/me');
                $user = $this->getEntityManager()->getRepository('Application\Entity\User');
                if (!$user->isUserRegistered($userProfile['email'])) {
                    $user->addUser($userProfile);
                } 
                return new ViewModel();
            } catch (FacebookApiException $e) {
                error_log($e);
                $user = null;
            }
        } else {
            $loginUrl = $facebook->getLoginUrl(array('scope' => 'email'));
            return $this->redirect()->toUrl($loginUrl);
        };             
    }
}