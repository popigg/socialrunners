<?php 

namespace Application\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class Run extends EntityRepository
{

    public function addRun($runInfo)
    {
    	$em = $this->getEntityManager();

        $run = new \Application\Entity\Run();
        $run->setUserId($runInfo->user_id);
    	$run->setPlace($runInfo->place);
        $run->setDate(new \DateTime($runInfo->date));
        $run->setTime(new \DateTime($runInfo->time));
        $run->setDistance($runInfo->distance);
        $run->setDuration($runInfo->duration);
        $run->setPace($runInfo->pace);
        $run->setMeetingPoint($runInfo->meetingPoint);
        $run->setDescription($runInfo->description);
        $run->setDrinks($runInfo->drinks);
        $run->setDateCreated(new \DateTime('now'));
        
        $em->persist($run);
        $em->flush();
    }

    public function listRun()
    {
        $ormList = $this->findAll();
        foreach ($ormList as $key => $value) {
            $list[] = array(
                'user_id'      => $value->getUserId(),
                'place'        => $value->getPlace(),
                'date'         => $value->getDate()->format('d-m-Y'),        
                'time'         => $value->getTime()->format('H:i:s'),        
                'distance'     => $value->getDistance(),        
                'duration'     => $value->getDuration(),
                'pace'         => $value->getPace(),        
                'meetingPoint' => $value->getMeetingPoint(),        
                'description'  => $value->getDescription(),        
                'drinks'       => $value->getDrinks(),  
            );      
        }
        return $list;
    }
}
