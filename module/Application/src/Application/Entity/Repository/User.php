<?php
namespace Application\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class User extends EntityRepository
{
	public function isUserRegistered($email)
    {
        $user = $this->findByEmail($email);
        if (!empty($user)) {
            return true;
        }
        return false;
    }

    public function addUser($userProfile)
    {
    	$em = $this->getEntityManager();

        $user = new \Application\Entity\User();
    	$user->setUsername($userProfile['username']);
        $user->setEmail($userProfile['email']);
        $user->setDisplayName($userProfile['name']);
        $user->setPassword(false);
        $user->setState(true);
        $user->setDateCreated(new \DateTime('now'));

        $em->persist($user);
        $em->flush();
    }
}
