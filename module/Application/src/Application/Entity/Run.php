<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Run Entity
 *
 * @ORM\Table(
 *   name="run",
 *   uniqueConstraints={
 *     @ORM\UniqueConstraint(name="id", columns={"id"})
 *   }
 * )
 * @ORM\Entity(repositoryClass="Application\Entity\Repository\Run")
 */

class Run 
{
	/**
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    protected $_id;

	/**
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    protected $_user_id;

    /**
     * @ORM\Column(name="place", type="string", nullable=false)
     */
    protected $_place;

    /**
     * @ORM\Column(name="date", type="date", nullable=false)
     */
    protected $_date;

    /**
     * @ORM\Column(name="time", type="time", nullable=false)
     */
    protected $_time;

    /**
     * @ORM\Column(name="distance", type="integer", nullable=true)
     */
    protected $_distance;

    /**
     * @ORM\Column(name="duration", type="integer", nullable=true)
     */
    protected $_duration;

    /**
     * @ORM\Column(name="pace", type="string", nullable=true)
     */
    protected $_pace;

    /**
     * @ORM\Column(name="track", type="string", nullable=true)
     */
    protected $_gpx;

    /**
     * @ORM\Column(name="meeting_point", type="string", nullable=true)
     */
    protected $_meetingPoint;

    /**
     * @ORM\Column(name="description", type="string", nullable=true)
     */
    protected $_description;

    /**
     * @ORM\Column(name="drinks", type="string", nullable=true)
     */
    protected $_drinks;

	/**
     * @ORM\Column(name="date_created", type="datetime")
     */
    protected $_dateCreated;

    /**
     * @ORM\Column(name="date_updated", type="datetime", nullable=true)
     */
    protected $_dateUpdated;

    public function getId()
    {
        return $this->_id;
    }

    public function getUserId()
    {
        return $this->_user_id;
    }

    public function getPlace() 
    {
        return $this->_place;
    }

    public function getDate()
    {
        return $this->_date;
    }

    public function getTime()
    {
        return $this->_time;
    }

    public function getDistance()
    {
        return $this->_distance;
    }

    public function getDuration()
    {
        return $this->_duration;
    }

    public function getPace()
    {
        return $this->_pace;
    }

    public function getGpx()
    {
        return $this->_gpx;
    }

    public function getMeetingPoint()
    {
        return $this->_meetingPoint;
    }

    public function getDescription()
    {
        return $this->_description;
    }

    public function getDrinks()
    {
        return $this->_drinks;
    }

    public function getDateCreated()
    {
        return $this->_dateCreated;
    }

    public function getDateUpdated()
    {
        return $this->_dateUpdated;
    }

    public function setUserId($user_id)
    {
        $this->_user_id = $user_id;
        return $this;
    }

    public function setPlace($place)
    {
        $this->_place = $place;
        return $this;
    }

    public function setDate($date)
    {
        $this->_date = $date;
        return $this;
    }

    public function setTime($time)
    {
        $this->_time = $time;
        return $this;
    }

    public function setDistance($distance)
    {
        $this->_distance = $distance;
        return $this;
    }

    public function setDuration($duration)
    {
        $this->_duration = $duration;
        return $this;
    }

    public function setPace($pace)
    {
        $this->_pace = $pace;
        return $this;
    }

    public function setGpx($gpx)
    {
        $this->_gpx = $gpx;
        return $this;
    }

    public function setMeetingPoint($meetingPoint)
    {
        $this->_meetingPoint = $meetingPoint;
        return $this;
    }

    public function setDescription($description)
    {
        $this->_description = $description;
        return $this;
    }

    public function setDrinks($drinks)
    {
        $this->_drinks = $drinks;
        return $this;
    }

    public function setDateCreated($dateCreated)
    {
        $this->_dateCreated = $dateCreated;
        return $this; 
    }

    public function setDateUpdated($dateUpdated)
    {
       $this->_dateUpdated = $dateUpdated;
        return $this; 
    }
}
