<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use ZfcUserDoctrineORM\Entity;

/**
 * User Entity
 *
 * @ORM\Table(
 *   name="user",
 *   uniqueConstraints={
 *     @ORM\UniqueConstraint(name="email", columns={"email"})
 *   }
 * )
 * @ORM\Entity(repositoryClass="Application\Entity\Repository\User")
 */

class User extends \ZfcUserDoctrineORM\Entity\User
{
    /**
     * @ORM\Column(name="date_created", type="datetime")
     */
    protected $_dateCreated;

    /**
     * @ORM\Column(name="date_updated", type="datetime", nullable=true)
     */
    protected $_dateUpdated;

    public function getDateCreated()
    {
        return $this->_dateCreated;
    }

    public function setDateCreated($dateCreated)
    {
       $this->_dateCreated = $dateCreated;
        return $this; 
    }

    public function getDateUpdated()
    {
        return $this->_dateUpdated;
    }

    public function setDateUpdated($dateUpdated)
    {
       $this->_dateUpdated = $dateUpdated;
        return $this; 
    }
}
